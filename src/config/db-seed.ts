import mongoose from 'mongoose'
import dotenv from 'dotenv'
import bcrypt from 'bcrypt'
import User, { ROLES } from '@/users/model'
import Barangay from '@/barangays/model'
import Citizen from '@/citizens/model'

dotenv.config();
(async () => {
  try {
    await mongoose.connect(process.env.DATABASE_URL!, { dbName: process.env.DATABASE_NAME! })
    console.log('connected to database')

    const salt = await bcrypt.genSalt(10)
    const password = await bcrypt.hash('password', salt)

    const users = await User.create({
      username: 'super_admin',
      password,
      email: 'john@doe.com',
      firstName: 'John',
      lastName: 'Doe',
      role: ROLES.SUPER_ADMIN
    }, {
      username: 'admin',
      password,
      email: 'jane@doe.com',
      firstName: 'Jane',
      lastName: 'Doe',
      role: ROLES.ADMIN
    })

    console.log('users collection seeded')

    const barangays = await Barangay.create({
      name: 'Amancoro',
      administrator: users[0]
    }, {
      name: 'Basing',
      administrator: users[1]
    })

    console.log('barangays collection seeded')

    // attach second barangay to admin user
    await User.findByIdAndUpdate(users[1]._id, { barangay: barangays[1]._id })

    console.log('attached barangay to admin user')

    await Citizen.create({
      firstName: 'Jennie',
      middleName: 'Middle',
      lastName: 'Kim',
      birthdate: '16/01/1996',
      contactNumber: '+639123456789',
      education: 'college',
      address: 'Amancoro chapel, Binmaley, Pangasinan',
      memberStatus: 'ACTIVE',
      recommendation: 'FOR_REGISTRATION',
      barangay: barangays[0],
      coordinates: {
        lat: 15.997963204721882,
        lng: 120.27716812341764
      }
    }, {
      firstName: 'Lalisa',
      middleName: 'Middle',
      lastName: 'Manobal',
      birthdate: '27/03/1997',
      contactNumber: '+639123456789',
      education: 'college',
      address: 'Amancoro chapel, Binmaley, Pangasinan',
      memberStatus: 'ACTIVE',
      recommendation: 'FOR_REGISTRATION',
      barangay: barangays[0],
      coordinates: {
        lat: 15.997963204721882,
        lng: 120.27716812341764
      }
    }, {
      firstName: 'Roseanne',
      middleName: 'Middle',
      lastName: 'Park',
      birthdate: '11/02/1997',
      contactNumber: '+639123456789',
      education: 'college',
      address: 'Brgy. Basing Chapel Church, Binmaley, Pangasinan',
      memberStatus: 'ACTIVE',
      recommendation: 'FOR_REGISTRATION',
      barangay: barangays[1],
      coordinates: {
        lat: 15.997963204721882,
        lng: 120.27716812341764
      }
    }, {
      firstName: 'Kim',
      middleName: 'Middle',
      lastName: 'Ji-soo',
      birthdate: '03/01/1995',
      contactNumber: '+639123456789',
      education: 'college',
      address: 'Brgy. Basing Chapel Church, Binmaley, Pangasinan',
      memberStatus: 'ACTIVE',
      recommendation: 'FOR_REGISTRATION',
      barangay: barangays[1],
      coordinates: {
        lat: 15.997963204721882,
        lng: 120.27716812341764
      }
    })

    console.log('citizens collection seeded')
    await mongoose.disconnect()
    console.log('database disconnected')
  } catch (error) {
    console.error('failed to seed database')
    console.error(error)
    process.exit(1)
  }
})()