import express from 'express'
import dotenv from 'dotenv'
import mongoose from 'mongoose'
import cors from 'cors'
import api from './routes'

dotenv.config();

const app = express()
app.use(express.json())
app.use(cors())
app.use(api)
const port = process.env.PORT

mongoose
  .connect(process.env.DATABASE_URL!, { dbName: process.env.DATABASE_NAME! })
  .then(() => console.log('connected to database'))
  .catch(console.error)

app.get('/', (req, res) => {
  res.status(200).send({ message: 'hello world!' })
})

app.listen(port, () => {
  console.log('Server is running at http://localhost:' + port)
})