import joi, { ObjectSchema, StringSchema } from 'joi'

export const objectIdSchema = joi.string().length(24).max(24, 'utf8').messages({ 'string.length': 'Invalid id' })

export const validateId = (id: string) => validate<string>(objectIdSchema, id)

export const validate = <T>(schema: ObjectSchema | StringSchema, payload: T): { payload: T, errors?: string } => {
  const { error, value } = schema.validate(payload, { abortEarly: true })
  return { payload: value, errors: error?.message }
}