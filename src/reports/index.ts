import { Router } from 'express'
import Citizen from '@/citizens/model'
import Barangay from '@/barangays/model'
import { StatusCodes } from 'http-status-codes'

const router = Router()

/**
 * Bar chart data for citizen status
 * active / inactive
 */
router.get('/bar/status/:barangayId', async (req, res) => {
  try {
    const barangay = await Barangay.findById(req.params.barangayId)
    if (!barangay) return res.status(StatusCodes.NOT_FOUND).json(`No matching record for ${req.params.barangayId}`)
    const result = await Citizen
      .aggregate()
      .match({ barangay: barangay._id })
      .group({ _id : "$hhStatus", total: { $sum: 1 } })
    const aggregation = result.reduce((acc, item) => {
      acc[item._id || 'NO_STATUS'] = item.total
      return acc
    }, {})
    const revoked = await Citizen.find({ barangay: barangay._id, revoked: true })
    aggregation['REVOKED'] = revoked.length || 0
    return res.json(aggregation)
  } catch (error) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

export default router