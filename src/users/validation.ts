import joi from 'joi'
import { SignUpPayload } from '@/auth/validation'
import { objectIdSchema, validate } from '@/helpers/validation'
import { ROLES } from './model'

export type UserUpdatePayload = Partial<SignUpPayload> & { _id: string, barangayId?: string }

export const validateUpdate = (payload: UserUpdatePayload) => {
  const schema = joi.object({
    _id: objectIdSchema.required(),
    username: joi.string(),
    password: joi.string(),
    email: joi.string().email(),
    firstName: joi.string(),
    lastName: joi.string(),
    role: joi.string().valid(...Object.values(ROLES)),
    barangayId: joi
      .any()
      .when('role', {
        is: joi.exist().equal(ROLES.ADMIN),
        then: objectIdSchema.required(),
        otherwise: objectIdSchema
      }),
  })

  return validate<typeof payload>(schema, payload)
}