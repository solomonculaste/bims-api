import { validateId } from '@/helpers/validation'
import { Router } from 'express'
import { StatusCodes } from 'http-status-codes'
import User from './model'
import Barangay from '@/barangays/model'
import { validateUpdate } from './validation'

const router = Router()

router.get('/', async (req, res) => {
  try {
    const users = await User.find()
    return res.json(users)
  } catch (error) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.get('/:id', async (req, res) => {
  try {
    const { payload: id, errors } = validateId(req.params.id)
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    const user = await User.findById(id)
    if (!user) return res.status(StatusCodes.NOT_FOUND).json(`No matching records for ${id}`)
    return res.json(user)
  } catch (error) {
    console.error(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.put('/:id', async (req, res) => {
  try {
    const { payload: { _id, barangayId, ...payload }, errors } = validateUpdate({ _id: req.params.id, ...req.body })
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    const barangay = await Barangay.findById(barangayId)
    if (barangay) payload.barangay = barangay
    const user = await User.findByIdAndUpdate(_id, payload, { new: true })
    if (!user) return res.status(StatusCodes.NOT_FOUND).json(`No matching records for ${_id}`)
    return res.json(user)
  } catch (error) {
    console.error(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.delete('/:id', async (req, res) => {
  try {
    const { payload: id, errors } = validateId(req.params.id)
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    const existing = await User.findByIdAndDelete(id)
    if (!existing) return res.status(StatusCodes.NOT_FOUND).json(`No matching records for ${id}`)
    return res.json('Successfully deleted ' + id)
  } catch (error) {
    console.error(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

export default router