import { Document, Schema, model } from 'mongoose'
import { Barangay } from '@/barangays/model'

export enum ROLES {
  SUPER_ADMIN = 'SUPER_ADMIN',
  ADMIN = 'ADMIN'
}

export interface User extends Document {
  username: string
  password: string
  role: ROLES,
  email?: string
  firstName?: string
  lastName?: string
  barangay?: Barangay
}

const schema = new Schema<User>({
  username: { type: String, required: true },
  password: { type: String, required: true },
  role: { type: String, enum: ROLES, required: true },
  firstName: { type: String },
  lastName: { type: String },
  email: { type: String },
  barangay: { type: Schema.Types.ObjectId, ref: 'Barangay' },
},
{
  collection: 'users',
  timestamps: true,
})

export default model('User', schema)