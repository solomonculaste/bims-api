import { Barangay } from '@/barangays/model';
import { Schema, model } from 'mongoose'
import { Household } from '../households/model';

export interface Citizen {
  firstName: string
  middleName: string
  lastName: string
  extensionName?: string
  birthdate: string
  contactNumber: string
  address: string
  previousAddresses?: string
  barangay: Barangay | string
  coordinates: {
    lat: number
    lng: number
  }
  hhId: string
  hhStatus: string
  recommendation: string
  dateOfEnumeration: string
  recommendationCategory?: string
  validatorName: string
  validatorSignature: string
  immediateSupervisorName: string
  immediateSupervisorSignature: string
  immediateSupervisorPosition: string
  beneficiaryName: string
  beneficiarySignature: string
  remarks?: string
  householdMembers: Household[],
  revoked?: boolean
}

const schema = new Schema({
  firstName: { type: String, required: true },
  middleName: { type: String, required: true },
  lastName: { type: String, required: true, },
  extensionName: { type: String },
  birthdate: { type: String, required: true },
  contactNumber: { type: String },
  address: { type: String, required: true },
  previousAddresses: { type: String },
  barangay: { type: Schema.Types.ObjectId, ref: 'Barangay' },
  coordinates: {
    lat: { type: Number, },
    lng: { type: Number, }
  },
  hhId: { type: String, required: true, },
  hhStatus: { type: String, required: true, },
  recommendation: { type: String, required: true, },
  recommendationCategory: { type: String },
  remarks: { type: String },
  dateOfEnumeration: { type: String, required: true },
  householdMembers: [{ type: Schema.Types.ObjectId, ref: 'Household' }],
  validatorName: { type: String },
  validatorSignature: { type: String },
  immediateSupervisorName: { type: String },
  immediateSupervisorSignature: { type: String },
  immediateSupervisorPosition: { type: String },
  beneficiaryName: { type: String },
  beneficiarySignature: { type: String },
  revoked: { type: String }
},
{
  collection: 'citizens',
  timestamps: true,
})

export default model('Citizen', schema)