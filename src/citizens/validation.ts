import joi from 'joi'
import { Citizen } from './model'
import { objectIdSchema, validate } from '@/helpers/validation'

export const validateQuery = (payload: Partial<Citizen & { _id: string, keywords: string }>) => {
  const schema = joi.object({
    _id: objectIdSchema,
    barangay: joi.string(),
    hhStatus: joi.string(),
    revoked: joi.string(),
    keywords: joi.string()
  })

  return validate<typeof payload>(schema, payload)
}

export const validateNew = (payload: Citizen & { barangayId: string, hhMemberIds: string[] }) => {
  const schema = joi.object({
    hhId: joi.string().required(),
    hhStatus: joi.string().required(),
    firstName: joi.string().required(),
    middleName: joi.string().required(),
    lastName: joi.string().required(),
    extensionName: joi.string().allow(''),
    birthdate: joi.date().required(),
    contactNumber: joi.string().required(),
    dateOfEnumeration: joi.date().required(),
    address: joi.string().required(),
    coordinates: joi.object({
      lat: joi.number().required(),
      lng: joi.number().required(),
    }),
    recommendation: joi.string().required(),
    recommendationCategory: joi.any().when('recommendation', {
      is: joi.exist().equal('RECOMMENDATION_CHOICE_06'),
      then: joi.string().required(),
    }),
    remarks: joi.string().allow(''),
    validatorSignature: joi.string().allow(''),
    validatorName: joi
      .any()
      .when('recommendation', {
        is: joi.exist().invalid('RECOMMENDATION_CHOICE_02'),
        then: joi.string().required()
      }),
    immediateSupervisorSignature: joi.string().allow(''),
    immediateSupervisorName: joi
      .any()
      .when('recommendation', {
        is: joi.exist().invalid('RECOMMENDATION_CHOICE_02'),
        then: joi.string().required()
      }),
    immediateSupervisorPosition: joi
      .any()
      .when('recommendation', {
        is: joi.exist().invalid('RECOMMENDATION_CHOICE_02'),
        then: joi.string().required()
      }),
    beneficiarySignature: joi.string().allow(''),
    beneficiaryName: joi
      .any()
      .when('recommendation', {
        is: joi.exist().invalid('RECOMMENDATION_CHOICE_02'),
        then: joi.string().required()
      }),
    barangayId: objectIdSchema.required(),
    hhMemberIds: joi.array().items(objectIdSchema),
    revoked: joi.boolean(),
  })
  

  return validate<typeof payload>(schema, payload)
}


export const validateUpdate = (payload: Partial<Citizen> & { _id: string, barangayId: string, hhMemberIds: string[] }) => {
  const schema = joi.object({
    _id: objectIdSchema,
    hhId: joi.string().allow(''),
    hhStatus: joi.string().allow(''),
    firstName: joi.string().allow(''),
    middleName: joi.string().allow(''),
    lastName: joi.string().allow(''),
    extensionName: joi.string().allow(''),
    birthdate: joi.date().allow(''),
    contactNumber: joi.string().allow(''),
    dateOfEnumeration: joi.date().allow(''),
    address: joi.string().allow(''),
    coordinates: joi.object({
      lat: joi.number().required(),
      lng: joi.number().required(),
    }),
    recommendation: joi.string().allow(''),
    recommendationCategory: joi.string().allow(''),
    remarks: joi.string().allow(''),
    validatorSignature: joi.string().allow(''),
    validatorName: joi.string().allow(''),
    immediateSupervisorSignature: joi.string().allow(''),
    immediateSupervisorName: joi.string().allow(''),
    immediateSupervisorPosition: joi.string().allow(''),
    beneficiarySignature: joi.string().allow(''),
    beneficiaryName: joi.string().allow(''),
    barangayId: objectIdSchema,
    hhMemberIds: joi.array().items(objectIdSchema),
    revoked: joi.boolean(),
  })

  return validate<typeof payload>(schema, payload)
}

export const validateFullName = (payload: Partial<Citizen>) => {
  const schema = joi.object({
    firstName: joi.string().required(),
    middleName: joi.string().required(),
    lastName: joi.string().required(),
    extensionName: joi.string(),
  })

  return validate<typeof payload>(schema, payload)
}