import { Router } from 'express'
import Citizen from './model'
import Household from '@/households/model'
import Barangay from '@/barangays/model'
import { StatusCodes } from 'http-status-codes'
import { validateQuery, validateNew, validateUpdate, validateFullName } from './validation'
import { validateId } from '@/helpers/validation'

const router = Router()

// role checking on these routes is at routes.ts file
router.get('/', async (req, res) => {
  try {
    const { payload: { barangay }, errors } = validateQuery(req.query)
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    const results = await Citizen.find({ barangay }).populate('barangay').populate('householdMembers')
    return res.json(results);
  } catch (error) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.get('/filtered', async (req, res) => {
  try {
    const { payload: { barangay, keywords, ...payload }, errors } = validateQuery(req.query)
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    const results = await Citizen.find({ barangay }).or([payload]).populate('barangay').populate('householdMembers')
    if (results.length && keywords) {
      const matched = results.filter(({ firstName, middleName, lastName, extensionName, address }) => {
        const fullName = `${firstName} ${middleName} ${lastName} ${extensionName} ${address}`
        return fullName.toLowerCase().includes(keywords?.toLowerCase()!)
      });
      return res.json(matched)
    }
    return res.json(results);
  } catch (error) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.get('/:id', async (req, res) => {
  try {
    const { payload: id, errors } = validateId(req.params.id)
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    const citizen = await Citizen.findById(id).populate('barangay').populate('householdMembers')
    if (!citizen) return res.status(StatusCodes.NOT_FOUND).json(`No matching record for ${req.params.id}`)
    return res.json(citizen)
  } catch (error) {
    console.error(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.post('/new', async (req, res) => {
  try {
    const { payload: { barangayId, hhMemberIds, ...payload }, errors } = validateNew(req.body)
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    const barangay = await Barangay.findById(barangayId)
    if (!barangay) return res.status(StatusCodes.NOT_FOUND).json(`No matching record for this barangay id ${barangayId}`)
    const householdMembers = await Household.find({ '_id': { $in: hhMemberIds } })
    if (householdMembers.length !== hhMemberIds.length) return res.status(StatusCodes.NOT_FOUND).json('Some household id were not found')
    if (payload.recommendation === 'RECOMMENDATION_CHOICE_02') payload.revoked = true;
    const record = await Citizen.create({ ...payload, barangay, householdMembers })
    return res.json(record)
  } catch (error) {
    console.log(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.put('/:id', async (req, res) => {
  try {
    const { payload: { _id, barangayId, hhMemberIds, ...payload }, errors } = validateUpdate({ _id: req.params.id, ...req.body })
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    if (payload.address) {
      const existing = await Citizen.findById(_id)
      const addresses = JSON.parse(existing?.previousAddresses || '[]') as string[]
      existing && !addresses.includes(existing.address) && addresses.push(existing.address)
      addresses.length && (payload.previousAddresses = JSON.stringify(addresses))
      existing && !addresses.includes(existing.address) && (payload.hhStatus = 'MEMBER_STATUS_CODE_03')
    }
    const householdMembers = await Household.find({ '_id': { $in: hhMemberIds } })
    if (hhMemberIds && householdMembers.length !== hhMemberIds.length) return res.status(StatusCodes.NOT_FOUND).json('Some household id were not found')
    if (payload.recommendation === 'RECOMMENDATION_CHOICE_02') payload.revoked = true;
    const citizen = await Citizen.findByIdAndUpdate(_id, { ...payload, barangay: barangayId, householdMembers: hhMemberIds ? householdMembers : undefined }, { new: true })
    return res.json(citizen)
  } catch (error) {
    console.error(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.delete('/:id', async (req, res) => {
  try {
    const { payload: id, errors } = validateId(req.params.id)
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    const existing = await Citizen.findByIdAndDelete(id)
    if (!existing) return res.status(StatusCodes.NOT_FOUND).json(`No matching records for ${id}`)
    return res.json(`Successfully deleted ${id}`)
  } catch (error) {
    console.error(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.delete('/batch-delete', async (req, res) => {
  try {
    await Citizen.deleteMany({ _id: { $in: req.body.ids } })
    return res.json('Records has been successfully deleted')
  } catch (error) {
    console.error(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.post('/check-hits', async (req, res) => {
  try {
    const { payload: { firstName, middleName, lastName, extensionName }, errors } = validateFullName(req.body)
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    const query = {
      firstName: { $regex: new RegExp(firstName!, 'i') },
      middleName: { $regex: new RegExp(middleName!, 'i') },
      lastName: { $regex: new RegExp(lastName!, 'i') },
    }
    
    // @ts-ignore
    if (extensionName) query.extensionName = { $regex: new RegExp(extensionName, 'i') }
    const records = await Citizen.find(query)
    return res.json(records)
  } catch (error) {
    console.error(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

export default router