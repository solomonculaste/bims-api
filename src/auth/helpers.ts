import { ROLES } from '@/users/model'
import { Request, Response, NextFunction } from 'express'
import { StatusCodes } from 'http-status-codes'
import JWT, { JwtPayload } from 'jsonwebtoken'
import User from '@/users/model'

export const verifyAuth = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = req.headers.authorization?.split(' ')[1]
    if (!token) throw new Error('Unauthorized')
    const decoded = JWT.verify(token, process.env.ACCESS_TOKEN_SECRET!)
    res.locals.userId = (decoded as JwtPayload).id
    next()
  } catch (error) {
    return res.status(StatusCodes.UNAUTHORIZED).json('Unauthorized')
  }
}

export const checkRole = (roles: ROLES[]) => async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = await User.findById(res.locals.userId)
    if (!user || !roles.includes(user.role)) throw new Error('Unauthorized')
    next()
  } catch (error) {
    return res.status(StatusCodes.UNAUTHORIZED).json('Unauthorized')
  }
}