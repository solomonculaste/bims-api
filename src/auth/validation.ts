import joi from 'joi'
import { validate } from '@/helpers/validation';
import { Barangay } from '@/barangays/model';

export interface LoginPayload {
  email?: string;
  username?: string;
  password: string;
}

export interface SignUpPayload {
  email: string;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  barangay?: Barangay
}

export const validateLogin = (payload: LoginPayload) => {
  const schema = joi.object({
    email: joi.string().email(),
    username: joi.string(),
    password: joi.string().required()
  }).or('email', 'username')

  return validate<LoginPayload>(schema, payload)
}

export const validateSignUp = (payload: SignUpPayload) => {
  const schema = joi.object({
    email: joi.string().email().required(),
    username: joi.string().required(),
    password: joi.string().required(),
    firstName: joi.string().required(),
    lastName: joi.string().required()
  })
  
  return validate<SignUpPayload>(schema, payload)
}
