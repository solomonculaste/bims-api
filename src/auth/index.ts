import { Router } from 'express'
import JWT from 'jsonwebtoken'
import bcrypt from 'bcrypt'
import User, { ROLES } from '@/users/model'
import { StatusCodes } from 'http-status-codes'
import { validateLogin, validateSignUp } from './validation'
import { checkRole, verifyAuth } from './helpers'

const router = Router()
export const createToken = (id: string) => JWT.sign({ id }, process.env.ACCESS_TOKEN_SECRET!)

router.post('/signup', verifyAuth, checkRole([ROLES.SUPER_ADMIN]), async (req, res) => {
  try {
    const { payload, errors } = validateSignUp(req.body)
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    let user = await User.findOne({ username: payload.username })
    if (user) return res.status(StatusCodes.BAD_REQUEST).json('username already exists')
    const salt = await bcrypt.genSalt(10)
    const password = await bcrypt.hash(payload.password, salt)
    user = await User.create({ ...payload, password })
    const token = createToken(user._id.toString())
    return res.json({ user, token })
  } catch (error) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.post('/login', async (req, res) => {
  try {
    const { payload: { username, password }, errors } = validateLogin(req.body)
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    const user = await User.findOne({ username }).populate('barangay')
    if (!user) return res.status(StatusCodes.UNAUTHORIZED).json('Invalid credentials')
    const auth = await bcrypt.compare(password, user.password)
    if (!auth) return res.status(StatusCodes.UNAUTHORIZED).json('Invalid credentials')
    delete (user as Partial<typeof user>).password
    res.json({ user, token: `Bearer ${createToken(user._id.toString())}` })
  } catch (error) {
    console.error(error)
    res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

export default router