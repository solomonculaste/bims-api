import { Router } from 'express'
import { checkRole, verifyAuth } from '@/auth/helpers'
import Auth from '@/auth'
import Users from '@/users'
import Barangays from '@/barangays'
import Citizens from '@/citizens'
import Households from '@/households'
import Reports from '@/reports'
import Misc from '@/misc'
import { ROLES } from './users/model'
const router = Router()

router.use('/auth', Auth)
router.use('/users', verifyAuth, checkRole([ROLES.SUPER_ADMIN]), Users)
router.use('/barangays', verifyAuth, Barangays)
router.use('/citizens', verifyAuth,  checkRole([ROLES.SUPER_ADMIN, ROLES.ADMIN]), Citizens)
router.use('/households', verifyAuth,  checkRole([ROLES.SUPER_ADMIN, ROLES.ADMIN]), Households)
router.use('/reports', verifyAuth, Reports)
router.use('/misc', Misc)

export default router