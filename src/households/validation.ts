import joi from 'joi'
import { validate } from '@/helpers/validation'
import { Household } from './model'

export const validateNew = (payload: Household) => {
  const schema = joi.object({
    _id: joi.string().allow(''),
    memberType: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    memberStatus: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    firstName: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    middleName: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    lastName: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    extensionName: joi.string().allow(''),
    birthdate: joi.date().required().messages({
      'date.empty': 'FIELD_IS_REQUIRED',
      'date.base': 'FIELD_IS_REQUIRED',
    }),
    age: joi
      .number()
      .min(0)
      .required()
      .messages({ 'number.empty': 'FIELD_IS_REQUIRED' }),
    gender: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    declaredPregnancyStatus: joi.any().when('gender', {
      is: joi.exist().equal('F'),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    pregnancyStatus: joi.any().when('gender', {
      is: joi.exist().equal('F'),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    lmp: joi.any().when('pregnancyStatus', {
      is: joi.exist().equal('PREGNANCY_STATUS_CODE_01'),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    pwd: joi.boolean().required().messages({
      'boolean.required': 'FIELD_IS_REQUIRED',
      'boolean.base': 'FIELD_IS_REQUIRED',
    }),
    relationshipToHH: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    soloParent: joi.boolean().required().messages({
      'boolean.required': 'FIELD_IS_REQUIRED',
      'boolean.base': 'FIELD_IS_REQUIRED',
    }),
    maritalStatus: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    ipMember: joi.boolean().required().messages({
      'boolean.required': 'FIELD_IS_REQUIRED',
      'boolean.base': 'FIELD_IS_REQUIRED',
    }),
    ipCustomName: joi.any().when('ipMember', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    mothersMaidenName: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    hhGrantee: joi.boolean().required().messages({
      'boolean.required': 'FIELD_IS_REQUIRED',
      'boolean.base': 'FIELD_IS_REQUIRED',
    }),
    visitingHF: joi.any().when('age', {
      is: joi.number().max(5),
      then: joi.boolean().required().messages({
        'boolean.required': 'FIELD_IS_REQUIRED',
        'boolean.base': 'FIELD_IS_REQUIRED',
      }),
    }),
    reasonForNotVisitingHF: joi.any().when('visitingHF', {
      is: joi.exist().equal(false),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    hfName: joi.any().when('visitingHF', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    hfAddress: joi.any().when('visitingHF', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    hfId: joi.any().when('visitingHF', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    philSysCardNo: joi.any().when('visitingHF', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    cvsEducation: joi.boolean().required().messages({
      'boolean.required': 'FIELD_IS_REQUIRED',
      'boolean.base': 'FIELD_IS_REQUIRED',
    }),
    attendingSchool: joi.boolean().required().messages({
      'boolean.required': 'FIELD_IS_REQUIRED',
      'boolean.base': 'FIELD_IS_REQUIRED',
    }),
    reasonForNotAttendingSchool: joi.any().when('attendingSchool', {
      is: joi.exist().equal(false),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    schoolName: joi.any().when('attendingSchool', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    schoolAddress: joi.any().when('attendingSchool', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    schoolFacilityId: joi.any().when('attendingSchool', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    lrn: joi.any().when('attendingSchool', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    education: joi.string().allow(''),
  })

  return validate<typeof payload>(schema, payload)
}