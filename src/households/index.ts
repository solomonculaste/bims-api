import { Router } from 'express'
import Household from './model'
// import { validateNew } from './validation'
import { StatusCodes } from 'http-status-codes'
import { validateId } from '@/helpers/validation'

const router = Router()

router.post('/new', async (req, res) => {
  try {
    const records = await Household.create(req.body)
    return res.json(records)
  } catch (error) {
    console.log(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.put('/batch-update', async (req, res) => {
  try {
    const items = []
    for (const payload of req.body) {
      const { _id, ...h } = payload
      const item = await Household.findByIdAndUpdate(_id, h)
      items.push(item)
    }
    return res.json(items)
  } catch (error) {
    console.error(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.delete('/:id', async (req, res) => {
  try {
    const { payload: id, errors } = validateId(req.params.id)
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    const existing = await Household.findByIdAndDelete(id)
    if (!existing) return res.status(StatusCodes.NOT_FOUND).json(`No matching records for ${id}`)
    return res.json(`Successfully deleted ${id}`)
  } catch (error) {
    console.error(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

export default router