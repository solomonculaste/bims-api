import { Schema, model } from 'mongoose'

export interface Household {
  firstName: string
  middleName: string
  lastName: string
  extensionName?: string
  birthdate: string
  gender: string
  memberType: string
  memberStatus: string
  declaredPregnancyStatus?: string
  pregnancyStatus?: string
  lmp?: boolean
  pwd?: boolean
  relationshipToHH?: string
  soloParent?: boolean
  maritalStatus?: string
  ipMember?: string
  ipCode?: string
  ipName?: string
  ipCustomName?: string
  mothersMaidenName?: string
  hhGrantee?: boolean
  visitingHF?: boolean
  reasonForNotVisitingHF?: string
  hfName?: string
  hfAddress?: string
  hfId?: string
  philSysCardNo?: string
  cvsEducation?: boolean
  attendingSchool?: string
  reasonForNotAttendingSchool?: string
  schoolName?: string
  schoolFacilityId?: string
  schoolAddress?: string
  lrn?: string
  education?: string
}

const schema = new Schema({
  firstName: { type: String, required: true },
  middleName: { type: String, required: true },
  lastName: { type: String, required: true },
  extensionName: { type: String },
  birthdate: { type: String, required: true },
  gender: { type: String, required: true },
  memberType: { type: String, required: true },
  memberStatus: { type: String, required: true },
  declaredPregnancyStatus: { type: String },
  pregnancyStatus: { type: String },
  lmp: { type: String },
  pwd: { type: Boolean },
  relationshipToHH: { type: String },
  soloParent: { type: Boolean },
  maritalStatus: { type: String, required: true },
  ipMember: { type: Boolean },
  ipCode: { type: String },
  ipName: { type: String },
  ipCustomName: { type: String },
  mothersMaidenName: { type: String },
  hhGrantee: { type: Boolean },
  visitingHF: { type: Boolean },
  reasonForNotVisitingHF: { type: String },
  hfName: { type: String },
  hfAddress: { type: String },
  hfId: { type: String },
  philSysCardNo: { type: String },
  cvsEducation: { type: Boolean },
  attendingSchool: { type: Boolean },
  reasonForNotAttendingSchool: { type: String },
  schoolName: { type: String },
  schoolFacilityId: { type: String },
  schoolAddress: { type: String },
  lrn: { type: String },
  education: { type: String },
},
{
  collection: 'households',
  timestamps: true,
})

export default model('Household', schema)