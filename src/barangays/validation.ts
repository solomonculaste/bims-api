import joi from 'joi'
import { Barangay } from './model'
import { objectIdSchema, validate } from '@/helpers/validation'

export const validateNew = (payload: Barangay & { adminPassword: string }) => {
  const schema = joi.object({
    name: joi.string().required(),
    adminPassword: joi.string().required(),
    coordinates: joi.object({
      lat: joi.number().required(),
      lng: joi.number().required()
    }).required(),
  })

  return validate<typeof payload>(schema, payload)
}

export const validateUpdate = (payload: Barangay & { _id: string }) => {
  const schema = joi.object({
    _id: objectIdSchema.required(),
    name: joi.string(),
    coordinates: joi.object({
      lat: joi.number(),
      lng: joi.number()
    }),
  })

  return validate<typeof payload>(schema, payload)
}