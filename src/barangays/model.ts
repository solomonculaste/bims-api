import { Schema, model, Document } from 'mongoose'
import { type User } from '@/users/model';

export interface Barangay extends Document {
  name: string;
  administrator: User,
  coordinates: {
    lat: number,
    lng: number
  }
}

const schema = new Schema<Barangay>({
  name: { type: String, required: true },
  administrator: { type: Schema.Types.ObjectId, ref: 'User' },
  coordinates: {
    lat: { type: Number, required: true },
    lng: { type: Number, required: true }
  }
},
{
  collection: 'barangays',
  timestamps: true,
})

export default model('Barangay', schema)