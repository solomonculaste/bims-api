import { Router } from 'express'
import Barangay from './model'
import User, { ROLES } from '@/users/model'
import { StatusCodes } from 'http-status-codes'
import { validateNew, validateUpdate } from './validation'
import { validateId } from '@/helpers/validation'
import { checkRole } from '@/auth/helpers'
import bcrypt from 'bcrypt'

const router = Router()

router.get('/', checkRole([ROLES.SUPER_ADMIN]), async (req, res) => {
  try {
    return res.json(await Barangay.find().sort('name').populate('administrator'))
  } catch (error) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.get('/:id', checkRole([ROLES.SUPER_ADMIN, ROLES.ADMIN]), async (req, res) => {
  try {
    const { payload: id, errors } = validateId(req.params.id)
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    const barangay = await Barangay.findById(id).populate('administrator')
    if (!barangay) return res.status(StatusCodes.NOT_FOUND).json(`No matching record for ${req.params.id}`)
    return res.json(barangay)
  } catch (error) {
    console.error(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.post('/new', checkRole([ROLES.SUPER_ADMIN]), async (req, res) => {
  try {
    const { payload: { adminPassword, ...payload }, errors } = validateNew(req.body)
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    // generate new admin user for this barangay
    const salt = await bcrypt.genSalt(10)
    const administrator = await User.create({
      username: `${payload.name.toLowerCase()}_admin`,
      password: await bcrypt.hash(adminPassword, salt),
      role: ROLES.ADMIN
    })
    const barangay = await Barangay.create({ ...payload, administrator })
    // attach new barangay to user
    await User.findByIdAndUpdate(administrator._id, { barangay })
    return res.json(barangay)
  } catch (error) {
    console.log(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.put('/:id', checkRole([ROLES.SUPER_ADMIN]), async (req, res) => {
  try {
    const { payload: { id, ...payload }, errors } = validateUpdate({ id: req.params.id, ...req.body })
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    const barangay = await Barangay.findByIdAndUpdate(id, payload, { new: true })
    return res.json(barangay)
  } catch (error) {
    console.error(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.delete('/:id', checkRole([ROLES.SUPER_ADMIN]), async (req, res) => {
  try {
    const { payload: id, errors } = validateId(req.params.id)
    if (errors) return res.status(StatusCodes.BAD_REQUEST).json(errors)
    const existing = await Barangay.findByIdAndDelete(id)
    if (!existing) return res.status(StatusCodes.NOT_FOUND).json(`No matching record for ${id}`)
    return res.json('Successfully deleted ' + id)
  } catch (error) {
    console.error(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

router.delete('/batch-delete', checkRole([ROLES.SUPER_ADMIN]), async (req, res) => {
  try {
    await Barangay.deleteMany({ _id: { $in: req.body.ids } })
    return res.json('Records has been successfully deleted')
  } catch (error) {
    console.error(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json('Something went wrong')
  }
})

export default router