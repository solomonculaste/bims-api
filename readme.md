# bims-api
###### Binmaley Indigent Management System API.

#### Setup

1. Make sure you are running the exact node version `18.16.0`.
2. Run `npm install`
3. Make sure you have mongodb up and running on your local machine.
4. Run `npm run seed` to persist dummy data in the database. (check `src/config/db-seed.ts` file for more info). Note: Make sure to empty `bims-db` database first if there is any existing entries to avoid duplicates.
5. Lastly run `npm run dev` to build and start the server.